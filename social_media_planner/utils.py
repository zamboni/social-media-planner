import calendar
from collections import defaultdict
import datetime

from dateutil.relativedelta import relativedelta

import models


def get_posts_calendar(start_date=None, **kwargs):
    """
    Kwargs:
     - `tot_months`: tot number of months (min 1)
     - `posts_filters`: dict of queryset filters
    """
    tot_months = kwargs.get('tot_months', 3)
    posts_filters = kwargs.get('posts_filters', {})
    if start_date is None:
        start_date = datetime.date.today()
    cal = calendar.Calendar()
    months = [start_date]
    for next_month in range(1, tot_months):
        months.append(start_date + relativedelta(months=+next_month))
    posts_calendar = {
        'months': []
    }
    for month in months:
        posts = defaultdict(list)
        for post in models.Post.objects.filter(publish_date__year=month.year,
                                               publish_date__month=month.month, **posts_filters):
            posts[post.publish_date.strftime('%Y-%m-%d')].append(post)
        for post in models.Post.objects.filter(publish_date__isnull=True,
                                               publish_date_from__month=month.month, **posts_filters):
            posts[post.publish_date_from.strftime('%Y-%m-%d')].append(post)
        for post in models.Post.objects.filter(publish_date__isnull=True, publish_date_from__isnull=True,
                                               publish_date_to__month=month.month, **posts_filters):
            posts[post.publish_date_to.strftime('%Y-%m-%d')].append(post)
        posts_calendar['months'].append({
            'weeks': [w for w in cal.monthdatescalendar(month.year, month.month)],
            'month_name': month.strftime('%B'),
            'month_number': int(month.strftime('%m')),
            'posts': posts,
        })
    return posts_calendar
