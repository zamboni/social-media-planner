import datetime

from django.core.mail import send_mail
from django.template import loader, Context

import bleach

import models, settings


def notification_action(action_name, user, target, **kwargs):
    if action_name == 'test_notification':
        notification_test_notification(user)
    if action_name == 'new_meeting':
        notification_new_meeting(user, target)
    if action_name == 'meeting_day':
        notification_meeting_day(user, target)
    if action_name == 'new_post':
        notification_new_post(user, target)
    if action_name == 'edit_my_post':
        notification_edit_my_post(user, target)
    if action_name == 'delete_post':
        notification_delete_post(user, target)
    if action_name == 'new_post_content':
        notification_new_post_content(user, target)
    if action_name == 'edit_my_post_content':
        notification_edit_my_post_content(user, target)
    if action_name == 'delete_my_post_content':
        notification_delete_my_post_content(user, target)
    if action_name == 'review_my_post_content':
        notification_review_my_post_content(user, target)
    if action_name == 'validate_my_post_content':
        notification_validate_my_post_content(user, target, **kwargs)
    if action_name == 'publish_my_post_content':
        notification_publish_my_post_content(user, target)
    if action_name == 'read_my_review':
        notification_read_my_review(user, target)
    if action_name == 'need_my_help':
        notification_need_my_help(user, target, **kwargs)
    if action_name == 'publish_post':
        notification_publish_post(user, target, **kwargs)
    if action_name == 'publish_post_reminder':
        notification_publish_post_reminder(user, target)
    if action_name == 'weekly_reminder':
        notification_weekly_reminder(user, target)


def send_notification(to, subject, message, text_message='', context=None):
    if not text_message:
        text_message = bleach.clean(message, strip=True)
    text_message += '\n\nSocial media planner - %s\n' % \
                    settings.SOCIAL_MEDIA_PLANNER['SITE_PREFIX']

    if context is None:
        context = {}
    html_backbone = loader.get_template('social_media_planner/materialize/' +
                                        'notifications/backbone.html')
    context.update({
        'subject': subject,
        'content': message,
        'SITE_PREFIX': settings.SOCIAL_MEDIA_PLANNER['SITE_PREFIX'],
    })
    backbone_context = Context(context)
    html_message = html_backbone.render(backbone_context)
    to_emails = [to_user.email for to_user in to]
    send_mail(subject, text_message, settings.SOCIAL_MEDIA_PLANNER['EMAIL_SENDER'],
              to_emails, html_message=html_message)


def notification_test_notification(user):
    subject = 'New notification'
    content = '{user} created a test email ' +\
              'with subject "New mail" and date {today_date}.'
    content = content.format(user=user.get_full_name(),
                             today_date=datetime.date.today())
    context = {
        'notification_button_url': settings.SOCIAL_MEDIA_PLANNER['SITE_PREFIX'],
        'notification_button_title': 'Post example',
    }
    send_notification([user], subject, content, '', context)


def notification_new_meeting(meeting_creator, meeting):
    profiles = models.UserProfile.objects.filter(notification_new_meeting=True)
    participants = meeting.participants.all()
    users = [p.user for p in profiles if p.user in participants]
    if users:
        subject = 'New meeting'
        content = '{user} created a new meeting on editorial calendar dashboard ' +\
                  'with title "{meeting_title}" and date {meeting_date}.<br />' + \
                  'Topics of the meeting are:<br />{meeting_topics}'

        content = content.format(user=meeting_creator.get_full_name(),
                                 meeting_title=meeting.title,
                                 meeting_date=meeting.meeting_date,
                                 meeting_topics=meeting.topics,
                                 )
        context = {
            'notification_button_url': settings.SOCIAL_MEDIA_PLANNER['SITE_PREFIX'] +
                                       meeting.get_absolute_url(),
            'notification_button_title': meeting.title,
        }
        send_notification(users, subject, content, '', context)


def notification_meeting_day(meeting_creator, meeting):
    profiles = models.UserProfile.objects.filter(notification_meeting_reminder=True)
    participants = meeting.participants.all()
    users = [p.user for p in profiles if p.user in participants]
    if users:
        subject = 'Meeting today!'
        content = 'This is a reminder for today\'s meeting. ' +\
                  'Meeting title is "{meeting_title}.'
        content = content.format(meeting_title=meeting.title)
        context = {
            'notification_button_url': settings.SOCIAL_MEDIA_PLANNER['SITE_PREFIX'] +
                                       meeting.get_absolute_url(),
            'notification_button_title': meeting.title,
        }
        send_notification(users, subject, content, '', context)


def notification_new_post(post_creator, post):
    profiles = models.UserProfile.objects.filter(notification_new_post=True)
    users = [p.user for p in profiles if p.user != post_creator]
    if users:
        subject = 'New post by {user}'.format(user=post_creator.get_full_name())
        content = '{user} created a new post on editorial calendar ' +\
                  'with label "{post_label}".'
        content = content.format(user=post_creator.get_full_name(),
                                 post_label=post.label)
        context = {
            'notification_button_url': settings.SOCIAL_MEDIA_PLANNER['SITE_PREFIX'] +
                                       post.get_absolute_url(),
            'notification_button_title': 'View post',
        }
        send_notification(users, subject, content, '', context)


def notification_edit_my_post(post_editor, post):
    if not post.created_by.social_media_planner_profile.notification_edit_my_post or post_editor == post.created_by:
        return

    users = [post.created_by]
    subject = 'Post "{post_label}" edited'.format(post_label=post.label)
    content = '{user} edited your post on editorial calendar.'
    content = content.format(user=post_editor.get_full_name(),
                             post_url=settings.SOCIAL_MEDIA_PLANNER['SITE_PREFIX'] +
                                post.get_absolute_url())
    context = {
        'notification_button_url': settings.SOCIAL_MEDIA_PLANNER['SITE_PREFIX'] +
                                   post.get_absolute_url(),
        'notification_button_title': 'View post',
    }
    send_notification(users, subject, content, '', context)


def notification_delete_post(post_editor, post):
    #  TODO
    profiles = models.UserProfile.objects.filter(notification_new_post=True)
    users = [p.user for p in profiles if p != post_editor]
    if users:
        subject = 'New post by {user}'.format(user=post_editor.get_full_name())
        content = '{user} created a new post on editorial calendar ' +\
                  'with label "{post_label}".'
        content = content.format(user=post_editor.get_full_name(),
                                 post_label=post.label)
        context = {
            'notification_button_url': settings.SOCIAL_MEDIA_PLANNER['SITE_PREFIX'] +
                                       post.get_absolute_url(),
            'notification_button_title': 'View post',
        }
        send_notification(users, subject, content, '', context)


def notification_new_post_content(post_content_creator, post_content):
    post = post_content.post
    if not post.created_by.social_media_planner_profile.notification_new_post_content or \
            post_content_creator == post.created_by:
        return

    users = [post.created_by]
    subject = 'Post "{post_label}" has new content'.format(post_label=post.label)
    content = '{user} added a post content ({post_content_type}) to your post.'
    content = content.format(user=post_content_creator.get_full_name(),
                             post_content_type=post_content.post_content_type)
    context = {
        'notification_button_url': settings.SOCIAL_MEDIA_PLANNER['SITE_PREFIX'] +
                                   post.get_absolute_url(),
        'notification_button_title': 'View post',
    }
    send_notification(users, subject, content, '', context)


def notification_edit_my_post_content(post_content_editor, post_content):
    post = post_content.post
    if not post_content.created_by.social_media_planner_profile.notification_edit_my_post_content or \
            post_content_editor == post_content.created_by:
        return

    users = [post_content.created_by]
    subject = 'Post content has been edited'
    content = '{user} edited your post content ({post_content_type}) on post "{post_label}".'
    content = content.format(user=post_content_editor.get_full_name(),
                             post_content_type=post_content.post_content_type,
                             post_label=post.label)
    context = {
        'notification_button_url': settings.SOCIAL_MEDIA_PLANNER['SITE_PREFIX'] +
                                   post.get_absolute_url(),
        'notification_button_title': 'View post',
    }
    send_notification(users, subject, content, '', context)


def notification_delete_my_post_content(post_creator, post):
    #  TODO
    profiles = models.UserProfile.objects.filter(notification_new_post=True)
    users = [p.user for p in profiles if p != post_creator]
    if users:
        subject = 'New post by {user}'.format(user=post_creator.get_full_name())
        content = '{user} created a new post on editorial calendar ' +\
                  'with label "{post_label}".'
        content = content.format(user=post_creator.get_full_name(),
                                 post_label=post.label)
        context = {
            'notification_button_url': settings.SOCIAL_MEDIA_PLANNER['SITE_PREFIX'] +
                                       post.get_absolute_url(),
            'notification_button_title': 'View post',
        }
        send_notification(users, subject, content, '', context)


def notification_review_my_post_content(post_content_review_creator, post_content_review):
    post_content = post_content_review.post_content
    post = post_content.post
    if not post_content.created_by.social_media_planner_profile.notification_review_my_post_content or \
            post_content_review_creator == post_content.created_by:
        return

    users = [post_content.created_by]
    subject = 'Post content has been reviewed'
    content = '{user} reviewed your post content ({post_content_type}) on post "{post_label}".'
    content = content.format(user=post_content_review_creator.get_full_name(),
                             post_content_type=post_content.post_content_type,
                             post_label=post.label)
    context = {
        'notification_button_url': settings.SOCIAL_MEDIA_PLANNER['SITE_PREFIX'] +
                                   post.get_absolute_url(),
        'notification_button_title': 'View post',
    }
    send_notification(users, subject, content, '', context)


def notification_validate_my_post_content(post_content_validator, post_content, **kwargs):
    type_name = kwargs['type_name']
    post = post_content.post
    if not post_content.created_by.social_media_planner_profile.notification_validate_my_post_content or \
            post_content_validator == post_content.created_by:
        return

    users = [post_content.created_by]
    subject = 'Post content has been validated'
    content = '{user} validated for "{type_name}" your post content ({post_content_type}) on post "{post_label}".'
    content = content.format(user=post_content_validator.get_full_name(),
                             type_name=type_name.replace('_', ' '),
                             post_content_type=post_content.post_content_type,
                             post_label=post.label)
    context = {
        'notification_button_url': settings.SOCIAL_MEDIA_PLANNER['SITE_PREFIX'] +
                                   post.get_absolute_url(),
        'notification_button_title': 'View post',
    }
    send_notification(users, subject, content, '', context)


def notification_publish_my_post_content(post_publisher, post_content):
    post = post_content.post
    if not post_content.created_by.social_media_planner_profile.notification_publish_my_post_content or \
            post_publisher == post_content.created_by:
        return

    users = [post_content.created_by]
    subject = 'Post content has been published'
    content = '{user} published your post content({post_content_type}) of post "{post_label}".'
    content = content.format(user=post_publisher.get_full_name(),
                             post_content_type=post_content.post_content_type,
                             post_label=post.label)
    context = {
        'notification_button_url': settings.SOCIAL_MEDIA_PLANNER['SITE_PREFIX'] +
                                   post.get_absolute_url(),
        'notification_button_title': 'View post',
    }
    send_notification(users, subject, content, '', context)


def notification_read_my_review(post_content_review_reader, post_content_review):
    post_content = post_content_review.post_content
    post = post_content.post
    if not post_content_review.created_by.social_media_planner_profile.notification_read_my_review or \
            post_content_review_reader == post_content_review.created_by:
        return

    users = [post_content_review.created_by]
    subject = 'Post content review has been read'
    content = '{user} has mark as read your review on post content({post_content_type}) of post "{post_label}".'
    content = content.format(user=post_content_review_reader.get_full_name(),
                             post_content_type=post_content.post_content_type,
                             post_label=post.label)
    context = {
        'notification_button_url': settings.SOCIAL_MEDIA_PLANNER['SITE_PREFIX'] +
                                   post.get_absolute_url(),
        'notification_button_title': 'View post',
    }
    send_notification(users, subject, content, '', context)


def notification_need_my_help(post_editor, post, **kwargs):
    previous_helpers = kwargs.get('previous_helpers', [])
    helpers = post.help_needed_from.all()
    if not helpers or list(helpers) == [post_editor]:
        return
    profiles = models.UserProfile.objects.filter(notification_need_my_help=True, user__in=helpers)
    users = [p.user for p in profiles if p.user != post_editor and p.user not in previous_helpers]
    if users:
        subject = 'Help required for a post'
        content = '{user} needs your help for a post on editorial calendar ' +\
                  'with label "{post_label}".'
        content = content.format(user=post_editor.get_full_name(),
                                 post_label=post.label)
        context = {
            'notification_button_url': settings.SOCIAL_MEDIA_PLANNER['SITE_PREFIX'] +
                                       post.get_absolute_url(),
            'notification_button_title': 'View post',
        }
        send_notification(users, subject, content, '', context)


def notification_publish_post(post_editor, post, **kwargs):
    previous_publishers = kwargs.get('previous_publishers', [])
    publishers = post.publishers.all()
    if not publishers or list(publishers) == [post_editor]:
        return
    profiles = models.UserProfile.objects.filter(notification_publish_post=True, user__in=publishers)
    users = [p.user for p in profiles if p.user != post_editor and p.user not in previous_publishers]
    if users:
        subject = 'Publish post'
        content = '{user} indicated your as a publisher for a post on editorial calendar ' +\
                  'with label "{post_label}" and publish date {publish_date}.'
        content = content.format(user=post_editor.get_full_name(),
                                 post_label=post.label,
                                 publish_date=post.publish_date.strftime('%Y-%m-%d %H:%M'))
        context = {
            'notification_button_url': settings.SOCIAL_MEDIA_PLANNER['SITE_PREFIX'] +
                                       post.get_absolute_url(),
            'notification_button_title': 'View post',
        }
        send_notification(users, subject, content, '', context)


def notification_publish_post_reminder(post_creator, post):
    publishers = post.publishers.all()
    if not publishers:
        return
    profiles = models.UserProfile.objects.filter(notification_publish_post_reminder=True, user__in=publishers)
    users = [p.user for p in profiles]
    if users:
        subject = 'You have to publish a post!'
        content = 'Remember that today at {publish_date} you have to publish the post ' +\
                  'with label "{post_label}".'
        content = content.format(post_label=post.label,
                                 publish_date=post.publish_date.strftime('%Y-%m-%d %H:%M'))
        context = {
            'notification_button_url': settings.SOCIAL_MEDIA_PLANNER['SITE_PREFIX'] +
                                       post.get_absolute_url(),
            'notification_button_title': 'View post',
        }
        send_notification(users, subject, content, '', context)


def notification_weekly_reminder(post_creator, post):
    #  TODO
    profiles = models.UserProfile.objects.filter(notification_new_post=True)
    users = [p.user for p in profiles if p != post_creator]
    if users:
        subject = 'New post by {user}'.format(user=post_creator.get_full_name())
        content = '{user} created a new post on editorial calendar ' +\
                  'with label "{post_label}".'
        content = content.format(user=post_creator.get_full_name(),
                                 post_label=post.label)
        context = {
            'notification_button_url': settings.SOCIAL_MEDIA_PLANNER['SITE_PREFIX'] +
                                       post.get_absolute_url(),
            'notification_button_title': 'View post',
        }
        send_notification(users, subject, content, '', context)

