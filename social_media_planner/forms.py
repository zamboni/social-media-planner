from django import forms
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext as _

from mptt.forms import TreeNodeChoiceField, TreeNodeMultipleChoiceField, TreeNodeChoiceFieldMixin
from multiupload.fields import MultiFileField

from django_helper_forms.fields import DateTimeSplitField, \
    UserModelChoiceField, UserModelMultipleChoiceField
from django_helper_forms.forms import DictionaryFieldsForm

import models


class DateInput(forms.DateInput):
    input_type = 'date'


class AttachmentForm(forms.ModelForm):
    delete = forms.BooleanField(required=False)
    is_image = forms.BooleanField(required=False)

    class Meta:
        model = models.Attachment
        fields = ('name', 'path', 'id', 'delete', 'is_image')


class MeetingForm(forms.ModelForm):
    #  meeting_date = DateTimeSplitField(date_attrs={'class': 'datepicker', 'type': 'date'})
    # meeting_date = DateTimeSplitField(date_attrs={'class': 'pickaday'},
    #                                   date_format='DD/MM/YYYY')
    participants = UserModelMultipleChoiceField(
        queryset=get_user_model().objects.exclude(
            first_name="").order_by('last_name', 'first_name'))

    class Meta:
        model = models.Meeting
        exclude = ('created_by', 'creation_date', 'last_edit_date', 'last_edit_by')
        widgets = {
            "meeting_date": forms.TextInput(attrs={'placeholder': 'YYYY-MM-DD hh:mm'}),
        }


class CategoryForm(forms.ModelForm):

    class Meta:
        model = models.Category
        exclude = ('codename', )


class KeyContentForm(forms.ModelForm):

    class Meta:
        model = models.KeyContent
        exclude = ('codename', )


class PostContentTypeForm(forms.ModelForm):

    class Meta:
        model = models.PostContentType
        exclude = ('codename', )


class PostForm(forms.ModelForm):
    label = forms.CharField(label='Short label', help_text='Max 150 chars.')
    key_content = TreeNodeChoiceField(queryset=models.KeyContent.objects.all())

    class Meta:
        model = models.Post
        exclude = ('created_by', 'creation_date', 'last_edit_date', 'last_edit_by', 'attachments')
        widgets = {
            "publish_date": DateInput(attrs={'placeholder': 'YYYY-MM-DD', 'class': 'pikaday'}),
            "publish_date_from": DateInput(attrs={'placeholder': 'YYYY-MM-DD', 'class': 'pikaday'}),
            "publish_date_to": DateInput(attrs={'placeholder': 'YYYY-MM-DD', 'class': 'pikaday'}),
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        status = cleaned_data['status']
        if status.codename != 'idea' and not \
                (cleaned_data['publish_date'] or cleaned_data['publish_date_from'] or
                     cleaned_data['publish_date_to']):
            raise forms.ValidationError("Please select a date (empty date is allowed only 'idea' posts)")
        return cleaned_data


class PostIdeaForm(forms.ModelForm):
    label = forms.CharField(label='Short label', help_text='Max 150 chars.')
    key_content = TreeNodeChoiceField(queryset=models.KeyContent.objects.all())
    status = forms.ModelChoiceField(
        queryset=models.ObjectStatus.objects.filter(model_class_name_tag='social_media_planner.Post.status'),
        widget=forms.HiddenInput())

    class Meta:
        model = models.Post
        fields = ('label', 'publish_date_from', 'publish_date_to', 'key_content', 'category', 'tags', 'post_reason',
                  'status')
        widgets={
            "publish_date_from": DateInput(attrs={'placeholder': 'YYYY-MM-DD', 'class': 'pikaday'}),
            "publish_date_to": DateInput(attrs={'placeholder': 'YYYY-MM-DD', 'class': 'pikaday'}),
        }


class PostContentForm(forms.ModelForm):

    class Meta:
        model = models.PostContent
        exclude = (
            'post',
            'created_by', 'creation_date', 'last_edit_date', 'last_edit_by',
            'content_checked', 'content_checked_by', 'content_checked_date',
            'grammar_checked', 'grammar_checked_by', 'grammar_checked_date',
            'post_content_type_checked', 'post_content_type_checked_by', 'post_content_type_checked_date',
            'publicated_by', 'publication_date',
        )


class PostContentReviewForm(forms.ModelForm):

    class Meta:
        model = models.PostContentReview
        exclude = (
            'post_content',
            'created_by', 'creation_date', 'last_edit_date', 'last_edit_by',
            'review_checked', 'review_checked_by', 'review_checked_date',
        )


class UserProfileNotificationsEditForm(forms.ModelForm):

    class Meta:
        model = models.UserProfile
        exclude = ('user',
                   'notification_weekly_reminder',
                   'notification_delete_my_post',
                   'notification_delete_my_post_content',
                   )


class AttachmentsForm(forms.Form):
    attachments = MultiFileField(min_num=1, max_num=3, max_file_size=1024 * 1024 * 3)
