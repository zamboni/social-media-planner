from django.conf.urls import url


from . import views

urlpatterns = [
    url(r'^$', views.SocialMediaPlannerView.as_view(), name='social_media_planner'),
    
    url(r'^notifications/send-test/$',
        views.SendTestNotificationView.as_view(), name='send_test_notification'),

    url(r'^user-profile/notifications/edit/$',
        views.UserProfileNotificationsEditView.as_view(), name='user_profile_notifications_edit'),

    url(r'^meeting/$', views.MeetingListView.as_view(), name='meeting_list'),
    url(r'^meeting/add/$', views.MeetingCreateView.as_view(), name='meeting_add',),
    url(r'^meeting/(?P<pk>\d+)/$', views.MeetingDetailView.as_view(), name='meeting_detail',),
    url(r'^meeting/(?P<pk>\d+)/edit/$', views.MeetingEditView.as_view(), name='meeting_edit',),
    url(r'^meeting/(?P<pk>\d+)/delete/$', views.MeetingDeleteView.as_view(), name='meeting_delete',),

    url(r'^category/$', views.CategoryListView.as_view(), name='category_list'),
    url(r'^category/add/$', views.CategoryCreateView.as_view(), name='category_add',),
    url(r'^category/(?P<pk>\d+)/$', views.CategoryDetailView.as_view(), name='category_detail',),
    url(r'^category/(?P<pk>\d+)/edit/$', views.CategoryEditView.as_view(), name='category_edit',),
    url(r'^category/(?P<pk>\d+)/delete/$', views.CategoryDeleteView.as_view(), name='category_delete',),

    url(r'^key-content/$', views.KeyContentListView.as_view(), name='key_content_list'),
    url(r'^key-content/add/$', views.KeyContentCreateView.as_view(), name='key_content_add',),
    url(r'^key-content/(?P<pk>\d+)/$', views.KeyContentDetailView.as_view(), name='key_content_detail',),
    url(r'^key-content/(?P<pk>\d+)/edit/$', views.KeyContentEditView.as_view(), name='key_content_edit',),
    url(r'^key-content/(?P<pk>\d+)/delete/$', views.KeyContentDeleteView.as_view(), name='key_content_delete',),

    url(r'^post-content-type/$', views.PostContentTypeListView.as_view(), name='post_content_type_list'),
    url(r'^post-content-type/add/$', views.PostContentTypeCreateView.as_view(), name='post_content_type_add',),
    url(r'^post-content-type/(?P<pk>\d+)/$', views.PostContentTypeDetailView.as_view(), name='post_content_type_detail',),
    url(r'^post-content-type/(?P<pk>\d+)/edit/$', views.PostContentTypeEditView.as_view(), name='post_content_type_edit',),
    url(r'^post-content-type/(?P<pk>\d+)/delete/$', views.PostContentTypeDeleteView.as_view(), name='post_content_type_delete',),

    url(r'^post/$', views.PostListView.as_view(), name='post_list'),
    url(r'^post/add/$', views.PostCreateView.as_view(), name='post_add',),
    url(r'^post/(?P<pk>\d+)/$', views.PostDetailView.as_view(), name='post_detail',),
    url(r'^post/(?P<pk>\d+)/edit/$', views.PostEditView.as_view(), name='post_edit',),
    url(r'^post/(?P<pk>\d+)/delete/$', views.PostDeleteView.as_view(), name='post_delete',),
    url(r'^post/(?P<pk>\d+)/attach-files/$', views.PostAttachFilesView.as_view(), name='post_attach_files',),
    url(r'^post/(?P<pk>\d+)/edit-attachments/$', views.PostEditAttachmentsView.as_view(), name='post_edit_attachments',),

    url(r'^post/(?P<post_id>\d+)/content/add/$', views.PostContentCreateView.as_view(), name='post_content_add',),
    url(r'^post/(?P<post_id>\d+)/content/(?P<pk>\d+)/edit/$', views.PostContentEditView.as_view(), name='post_content_edit',),
    url(r'^post/(?P<post_id>\d+)/content/(?P<pk>\d+)/delete/$', views.PostContentDeleteView.as_view(), name='post_content_delete',),
    url(r'^post/(?P<post_id>\d+)/content/(?P<pk>\d+)/validate/(?P<type_name>grammar|content|post_content_type)/$',
        views.PostContentValidateView.as_view(), name='post_content_validate',),
    url(r'^post/(?P<post_id>\d+)/content/(?P<pk>\d+)/invalidate/(?P<type_name>grammar|content|post_content_type)/$',
        views.PostContentInvalidateView.as_view(), name='post_content_invalidate',),
    url(r'^post/(?P<post_id>\d+)/content/(?P<pk>\d+)/publish/$',
        views.PostContentPublishView.as_view(), name='post_content_publish',),

    url(r'^post/(?P<post_id>\d+)/content/(?P<post_content_id>\d+)/review/add/$',
        views.PostContentReviewCreateView.as_view(), name='post_content_review_add',),
    url(r'^post/(?P<post_id>\d+)/content/(?P<post_content_id>\d+)/review/(?P<pk>\d+)/edit/$',
        views.PostContentReviewEditView.as_view(), name='post_content_review_edit',),
    url(r'^post/(?P<post_id>\d+)/content/(?P<post_content_id>\d+)/review/(?P<pk>\d+)/delete/$',
        views.PostContentReviewDeleteView.as_view(), name='post_content_review_delete',),
    url(r'^post/(?P<post_id>\d+)/content/(?P<post_content_id>\d+)/review/(?P<pk>\d+)/validate/(?P<type_name>review)/$',
        views.PostContentReviewValidateView.as_view(), name='post_content_review_validate',),
    url(r'^post/(?P<post_id>\d+)/content/(?P<post_content_id>\d+)/review/(?P<pk>\d+)/invalidate/(?P<type_name>review)/$',
        views.PostContentReviewInvalidateView.as_view(), name='post_content_review_invalidate',),
    url(r'^post/(?P<post_id>\d+)/content/(?P<post_content_id>\d+)/review/(?P<pk>\d+)/set-as-content/$',
        views.PostContentReviewSetAsContentView.as_view(), name='post_content_review_set_as_content',),
]