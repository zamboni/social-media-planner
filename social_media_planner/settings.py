from django.conf import settings


SOCIAL_MEDIA_PLANNER_SETTINGS = getattr(settings, 'SOCIAL_MEDIA_PLANNER', {})


SOCIAL_MEDIA_PLANNER = {
    'TEMPLATE_STYLE': SOCIAL_MEDIA_PLANNER_SETTINGS.get('TEMPLATE_STYLE', 'materialize'),

    'GROUPS': {
        'superuser': SOCIAL_MEDIA_PLANNER_SETTINGS.get('GROUPS', {}).get('superuser', 'social_media_planner_superuser'),
        'content_supervisor': SOCIAL_MEDIA_PLANNER_SETTINGS.get('GROUPS', {}).get('content_supervisor', 'social_media_planner_content_supervisor'),
        'publisher': SOCIAL_MEDIA_PLANNER_SETTINGS.get('GROUPS', {}).get('publisher', 'social_media_planner_publisher'),
        'meeting_manager': SOCIAL_MEDIA_PLANNER_SETTINGS.get('GROUPS', {}).get('meeting_manager', 'social_media_planner_meeting_manager'),
        'member': SOCIAL_MEDIA_PLANNER_SETTINGS.get('GROUPS', {}).get('member', 'social_media_planner_member'),
    },

    'PATHS': {
        'general': SOCIAL_MEDIA_PLANNER_SETTINGS.get('PATHS', {}).get('general', 'general'),
        'meeting': SOCIAL_MEDIA_PLANNER_SETTINGS.get('PATHS', {}).get('meeting', 'meeting/{id}'),
        'post': SOCIAL_MEDIA_PLANNER_SETTINGS.get('PATHS', {}).get('post', 'post/{id}'),
    },

    'SITE_PREFIX': SOCIAL_MEDIA_PLANNER_SETTINGS.get('SITE_PREFIX', 'http://localhost:8000'),
    'EMAIL_SENDER': SOCIAL_MEDIA_PLANNER_SETTINGS.get('EMAIL_SENDER', 'editorial_calendar@example.com'),
    'GET_ACCOUNT_SETTINGS_URL': SOCIAL_MEDIA_PLANNER_SETTINGS.get('GET_ACCOUNT_SETTINGS_URL', lambda user: 'account_settings')
}

if SOCIAL_MEDIA_PLANNER['TEMPLATE_STYLE'] != '':
    SOCIAL_MEDIA_PLANNER['TEMPLATE_STYLE'] = '/' + SOCIAL_MEDIA_PLANNER['TEMPLATE_STYLE']
