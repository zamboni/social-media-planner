from django.contrib.auth.models import Group, Permission

from social_media_planner import models, settings


def group(name, permissions=[]):
    obj, _ = Group.objects.get_or_create(name=name)
    for perm in permissions:
        obj.permissions.add(perm)


def load_initial_features():
    """Load groups with relative permissions"""
    
    # GET PERMISSIONS
    permissions = {
        'category': {
            'add': Permission.objects.get_by_natural_key('add_category', 'social_media_planner', 'category'),
            'change': Permission.objects.get_by_natural_key('change_category', 'social_media_planner', 'category'),
            'delete': Permission.objects.get_by_natural_key('delete_category', 'social_media_planner', 'category'),
        },
        'post_content_type': {
            'add': Permission.objects.get_by_natural_key('add_postcontenttype', 'social_media_planner', 'postcontenttype'),
            'change': Permission.objects.get_by_natural_key('change_postcontenttype', 'social_media_planner', 'postcontenttype'),
            'delete': Permission.objects.get_by_natural_key('delete_postcontenttype', 'social_media_planner', 'postcontenttype'),
        },
        'meeting': {
            'add': Permission.objects.get_by_natural_key('add_meeting', 'social_media_planner', 'meeting'),
            'change': Permission.objects.get_by_natural_key('change_meeting', 'social_media_planner', 'meeting'),
            'delete': Permission.objects.get_by_natural_key('delete_meeting', 'social_media_planner', 'meeting'),
        },
        'key_content': {
            'add': Permission.objects.get_by_natural_key('add_keycontent', 'social_media_planner', 'keycontent'),
            'change': Permission.objects.get_by_natural_key('change_keycontent', 'social_media_planner', 'keycontent'),
            'delete': Permission.objects.get_by_natural_key('delete_keycontent', 'social_media_planner', 'keycontent'),
        },
        'post': {
            'add': Permission.objects.get_by_natural_key('add_post', 'social_media_planner', 'post'),
            'change': Permission.objects.get_by_natural_key('change_post', 'social_media_planner', 'post'),
            'delete': Permission.objects.get_by_natural_key('delete_post', 'social_media_planner', 'post'),
        },
        'post_content': {
            'add': Permission.objects.get_by_natural_key('add_postcontent', 'social_media_planner', 'postcontent'),
            'change': Permission.objects.get_by_natural_key('change_postcontent', 'social_media_planner', 'postcontent'),
            'delete': Permission.objects.get_by_natural_key('delete_postcontent', 'social_media_planner', 'postcontent'),
        },
    }

    superuser = []
    for model in permissions.values():
        superuser += model.values()
    group(settings.SOCIAL_MEDIA_PLANNER['GROUPS']['superuser'], superuser)

    content_supervisor_models = ['category', 'post_content_type', 'key_content', 'post', 'post_content']
    content_supervisor = [
        Permission.objects.get_by_natural_key('can_validate_post_grammar', 'social_media_planner', 'post'),
        Permission.objects.get_by_natural_key('can_validate_post_content', 'social_media_planner', 'post'),
        Permission.objects.get_by_natural_key('can_manage_post_content_reviews', 'social_media_planner', 'post'),
    ]
    for model_name in content_supervisor_models:
        content_supervisor += permissions[model_name].values()
    group(settings.SOCIAL_MEDIA_PLANNER['GROUPS']['content_supervisor'], content_supervisor)

    publisher = [
        Permission.objects.get_by_natural_key('can_validate_post_post_content_type', 'social_media_planner', 'post'),
        Permission.objects.get_by_natural_key('can_manage_post_content_reviews', 'social_media_planner', 'post'),
        Permission.objects.get_by_natural_key('can_publish_post', 'social_media_planner', 'post'),
    ]
    group(settings.SOCIAL_MEDIA_PLANNER['GROUPS']['publisher'], publisher)

    member = [
        Permission.objects.get_by_natural_key('add_post', 'social_media_planner', 'post'),
        Permission.objects.get_by_natural_key('change_post', 'social_media_planner', 'post'),
        Permission.objects.get_by_natural_key('add_postcontent', 'social_media_planner', 'postcontent'),
        Permission.objects.get_by_natural_key('change_postcontent', 'social_media_planner', 'postcontent'),
    ]
    group(settings.SOCIAL_MEDIA_PLANNER['GROUPS']['member'], member)

    meeting_manager_models = ['meeting', ]
    meeting_manager = []
    for model_name in meeting_manager_models:
        meeting_manager += permissions[model_name].values()
    group(settings.SOCIAL_MEDIA_PLANNER['GROUPS']['meeting_manager'], meeting_manager)

    models.ObjectStatus.objects.get_or_create(label='Idea', codename='idea',
        model_class_name_tag='social_media_planner.Post.status', order=1)
    models.ObjectStatus.objects.get_or_create(label='Draft', codename='draft',
        model_class_name_tag='social_media_planner.Post.status', order=2)
    models.ObjectStatus.objects.get_or_create(label='Ready', codename='ready',
        model_class_name_tag='social_media_planner.Post.status', order=3)
    models.ObjectStatus.objects.get_or_create(label='Published', codename='published',
        model_class_name_tag='social_media_planner.Post.status', order=4)

    models.ObjectStatus.objects.get_or_create(label='General', codename='general',
        model_class_name_tag='social_media_planner.Attachment.category', order=1)
    models.ObjectStatus.objects.get_or_create(label='Meeting', codename='meeting',
        model_class_name_tag='social_media_planner.Attachment.category', order=2)
    models.ObjectStatus.objects.get_or_create(label='Post', codename='post',
        model_class_name_tag='social_media_planner.Attachment.category', order=3)
