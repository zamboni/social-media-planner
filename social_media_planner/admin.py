from django.contrib import admin

import models

admin.site.register(models.Attachment)

admin.site.register(models.Meeting)

admin.site.register(models.KeyContent)
admin.site.register(models.Category)

admin.site.register(models.Post)

admin.site.register(models.PostContentType)
admin.site.register(models.PostContent)
