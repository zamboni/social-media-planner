from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, Permission

from social_media_planner import models, settings
from social_media_planner.fixtures_module import initial_features


def get_group(name):
    return Group.objects.get(name=name)


def load_data():
    initial_features.load_initial_features()

    superuser_g = get_group(settings.SOCIAL_MEDIA_PLANNER['GROUPS']['superuser'])
    content_supervisor_g = get_group(settings.SOCIAL_MEDIA_PLANNER['GROUPS']['content_supervisor'])
    publisher_g = get_group(settings.SOCIAL_MEDIA_PLANNER['GROUPS']['publisher'])
    member_g = get_group(settings.SOCIAL_MEDIA_PLANNER['GROUPS']['member'])

    UserModel = get_user_model()

    superuser, _ = UserModel.objects.get_or_create(username='superuser',
        first_name='Mary', last_name='Superuser', email='superuser@example.com')
    superuser.groups.add(superuser_g)
    superuser.set_password('superuser_pass_123')
    superuser.save()

    content_supervisor, _ = UserModel.objects.get_or_create(username='editor',
        first_name='Jane', last_name='Editor', email='editor@example.com')
    content_supervisor.groups.add(member_g)
    content_supervisor.groups.add(content_supervisor_g)
    content_supervisor.set_password('editor_pass_123')
    content_supervisor.save()

    publisher, _ = UserModel.objects.get_or_create(username='publisher',
        first_name='Mike', last_name='Publisher', email='publisher@example.com')
    publisher.groups.add(member_g)
    publisher.groups.add(content_supervisor_g)
    publisher.groups.add(publisher_g)
    publisher.set_password('publisher_pass_123')
    publisher.save()

    member, _ = UserModel.objects.get_or_create(username='member',
        first_name='John', last_name='Member', email='member@example.com')
    member.groups.add(member_g)
    member.set_password('member_pass_123')
    member.save()

    models.Category.objects.get_or_create(label='Events')
    models.Category.objects.get_or_create(label='Products launch')
    models.Category.objects.get_or_create(label='Seminars')

    company, _ = models.KeyContent.objects.get_or_create(label='Company', key_content_color='#9fa8da', key_content_text_color='#000000')
    company_internal, _ = models.KeyContent.objects.get_or_create(label='Internal', key_content_color='#7986cb', parent=company)
    models.KeyContent.objects.get_or_create(label='People', parent=company_internal)
    models.KeyContent.objects.get_or_create(label='Works', parent=company_internal)
    softwares, _ = models.KeyContent.objects.get_or_create(label='Softwares', key_content_color='#c5e1a5', key_content_text_color='#000000')
    front_end, _ = models.KeyContent.objects.get_or_create(label='Front-end technologies', key_content_color='#aed581', parent=softwares)
    models.KeyContent.objects.get_or_create(label='Angular', parent=front_end)
    models.KeyContent.objects.get_or_create(label='React', parent=front_end)
    back_end, _ = models.KeyContent.objects.get_or_create(label='Back-end technologies', key_content_color='#9ccc65', parent=softwares)
    models.KeyContent.objects.get_or_create(label='Django', parent=back_end)
    models.KeyContent.objects.get_or_create(label='Ruby on Rails', parent=back_end)

    models.PostContentType.objects.get_or_create(label='Facebook', icon='fa-facebook')
    models.PostContentType.objects.get_or_create(label='Youtube', icon='fa-youtube')
    models.PostContentType.objects.get_or_create(label='Twitter', icon='fa-twitter')
    models.PostContentType.objects.get_or_create(label='LinkedIN', icon='fa-linkedin')
