# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-21 11:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('social_media_planner', '0006_post_publishers'),
    ]

    operations = [
        migrations.AddField(
            model_name='keycontent',
            name='key_content_text_color',
            field=models.CharField(blank=True, default='#FFFFFF', help_text='Reverse color (if color is used as background), default is white', max_length=7),
        ),
    ]
