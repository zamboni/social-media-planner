import datetime
import mimetypes

from django.contrib.auth import get_user_model
from django.conf import settings as dj_settings
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.forms import modelformset_factory
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import ListView, DetailView, \
    CreateView, UpdateView, DeleteView, FormView, TemplateView, View

from braces.views import LoginRequiredMixin, JSONResponseMixin
from model_attachments.views import AttachmentFileUploadReceiveMixin

from social_media_planner import models, settings, forms, notifications


TS = settings.SOCIAL_MEDIA_PLANNER['TEMPLATE_STYLE']


class SocialMediaPlannerView(TemplateView):
    template_name = 'social_media_planner%s/social_media_planner.html' % TS

    def get_context_data(self, **kwargs):
        context = super(SocialMediaPlannerView, self).get_context_data(**kwargs)
        return context


class SendTestNotificationView(TemplateView):
    template_name = 'social_media_planner%s/notifications/backbone.html' % TS

    def get_context_data(self, **kwargs):
        context = super(SendTestNotificationView, self).get_context_data(**kwargs)
        context.update({
            'subject': 'Test notification',
            'content': 'Content will be placed here. An email has been sent to %s.' %
                        self.request.user.email,
            'notification_button_url': settings.SOCIAL_MEDIA_PLANNER['SITE_PREFIX'],
            'notification_button_title': 'Social media planner',
            'SITE_PREFIX': settings.SOCIAL_MEDIA_PLANNER['SITE_PREFIX'],
        })
        notifications.notification_action('test_notification', self.request.user, None)
        return context


"""
Attachments
"""


"""
Meetings
"""


class MeetingMixin(object):
    context_object_name = 'meeting'
    model = models.Meeting
    form_class = forms.MeetingForm

    def get_success_url(self):
        return reverse('social_media_planner:meeting_list')


class MeetingListView(LoginRequiredMixin, MeetingMixin, ListView):
    template_name = 'social_media_planner%s/meeting/meeting_list.html' % TS

    def get_queryset(self):
        return models.Meeting.objects.all()


class MeetingDetailView(LoginRequiredMixin, MeetingMixin, DetailView):
    template_name = 'social_media_planner%s/meeting/meeting_detail.html' % TS


class MeetingCreateView(LoginRequiredMixin, MeetingMixin, CreateView):
    template_name = 'social_media_planner%s/meeting/meeting_form.html' % TS

    def form_valid(self, form):
        meeting = form.save(commit=False)
        meeting.audit_user = self.request.user
        meeting.save()
        form.save_m2m()
        notifications.notification_action('new_meeting', self.request.user, meeting)
        return HttpResponseRedirect(meeting.get_absolute_url())


class MeetingEditView(LoginRequiredMixin, MeetingMixin, UpdateView):
    template_name = 'social_media_planner%s/meeting/meeting_form.html' % TS

    def form_valid(self, form):
        meeting = form.save(commit=False)
        meeting.audit_user = self.request.user
        meeting.save()
        form.save_m2m()
        return HttpResponseRedirect(meeting.get_absolute_url())


class MeetingDeleteView(LoginRequiredMixin, MeetingMixin, DeleteView):
    template_name = 'social_media_planner%s/meeting/meeting_confirm_delete.html' % TS
    
    
"""
Categories
"""


class CategoryMixin(object):
    context_object_name = 'category'
    model = models.Category
    form_class = forms.CategoryForm

    def get_success_url(self):
        return reverse('social_media_planner:category_list')


class CategoryListView(LoginRequiredMixin, CategoryMixin, ListView):
    template_name = 'social_media_planner%s/category/category_list.html' % TS

    def get_queryset(self):
        return models.Category.objects.all()


class CategoryDetailView(LoginRequiredMixin, CategoryMixin, DetailView):
    template_name = 'social_media_planner%s/category/category_detail.html' % TS


class CategoryCreateView(LoginRequiredMixin, CategoryMixin, CreateView):
    template_name = 'social_media_planner%s/category/category_form.html' % TS

    def form_valid(self, form):
        category = form.save()
        return HttpResponseRedirect(category.get_absolute_url())


class CategoryEditView(LoginRequiredMixin, CategoryMixin, UpdateView):
    template_name = 'social_media_planner%s/category/category_form.html' % TS

    def form_valid(self, form):
        category = form.save()
        return HttpResponseRedirect(category.get_absolute_url())


class CategoryDeleteView(LoginRequiredMixin, CategoryMixin, DeleteView):
    template_name = 'social_media_planner%s/category/category_confirm_delete.html' % TS


"""
Key Contents
"""


class KeyContentMixin(object):
    context_object_name = 'key_content'
    model = models.KeyContent
    form_class = forms.KeyContentForm

    def get_success_url(self):
        return reverse('social_media_planner:key_content_list')


class KeyContentListView(LoginRequiredMixin, KeyContentMixin, ListView):
    template_name = 'social_media_planner%s/key_content/key_content_list.html' % TS

    def get_queryset(self):
        return models.KeyContent.objects.all()


class KeyContentDetailView(LoginRequiredMixin, KeyContentMixin, DetailView):
    template_name = 'social_media_planner%s/key_content/key_content_detail.html' % TS


class KeyContentCreateView(LoginRequiredMixin, KeyContentMixin, CreateView):
    template_name = 'social_media_planner%s/key_content/key_content_form.html' % TS

    def form_valid(self, form):
        key_content = form.save()
        return HttpResponseRedirect(key_content.get_absolute_url())


class KeyContentEditView(LoginRequiredMixin, KeyContentMixin, UpdateView):
    template_name = 'social_media_planner%s/key_content/key_content_form.html' % TS

    def form_valid(self, form):
        key_content = form.save()
        return HttpResponseRedirect(key_content.get_absolute_url())


class KeyContentDeleteView(LoginRequiredMixin, KeyContentMixin, DeleteView):
    template_name = 'social_media_planner%s/key_content/key_content_confirm_delete.html' % TS


"""
PostContentTypes
"""


class PostContentTypeMixin(object):
    context_object_name = 'post_content_type'
    model = models.PostContentType
    form_class = forms.PostContentTypeForm

    def get_success_url(self):
        return reverse('social_media_planner:post_content_type_list')


class PostContentTypeListView(LoginRequiredMixin, PostContentTypeMixin, ListView):
    template_name = 'social_media_planner%s/post_content_type/post_content_type_list.html' % TS

    def get_queryset(self):
        return models.PostContentType.objects.all()


class PostContentTypeDetailView(LoginRequiredMixin, PostContentTypeMixin, DetailView):
    template_name = 'social_media_planner%s/post_content_type/post_content_type_detail.html' % TS


class PostContentTypeCreateView(LoginRequiredMixin, PostContentTypeMixin, CreateView):
    template_name = 'social_media_planner%s/post_content_type/post_content_type_form.html' % TS

    def form_valid(self, form):
        post_content = form.save()
        return HttpResponseRedirect(post_content.get_absolute_url())


class PostContentTypeEditView(LoginRequiredMixin, PostContentTypeMixin, UpdateView):
    template_name = 'social_media_planner%s/post_content_type/post_content_type_form.html' % TS

    def form_valid(self, form):
        post_content = form.save()
        return HttpResponseRedirect(post_content.get_absolute_url())


class PostContentTypeDeleteView(LoginRequiredMixin, PostContentTypeMixin, DeleteView):
    template_name = 'social_media_planner%s/post_content_type/' % TS + \
                    'post_content_type_confirm_delete.html'


"""
Posts
"""


class PostMixin(object):
    context_object_name = 'post'
    model = models.Post
    form_class = forms.PostForm

    def get_success_url(self):
        return reverse('social_media_planner:post_list')


class PostListView(LoginRequiredMixin, PostMixin, ListView):
    template_name = 'social_media_planner%s/post/post_list.html' % TS
    paginate_by = 20

    def get_queryset(self):
        filtered = False
        queryset = models.Post.objects.all().order_by('publish_date')
        if self.request.GET.get('only_not_published') == '1':
            queryset = queryset.filter(Q(post_contents__publication_date__isnull=True) |
                                       Q(post_contents__isnull=True)).distinct()
            filtered = True
        if self.request.GET.get('only_published') == '1':
            queryset = queryset.filter(post_contents__publication_date__isnull=False).distinct()
            filtered = True
        if self.request.GET.get('only_mine') == '1':
            queryset = queryset.filter(created_by=self.request.user)
            filtered = True
        if self.request.GET.get('post_created_by'):
            queryset = queryset.filter(
                created_by__username=self.request.GET.get('post_created_by')).distinct()
            filtered = True
        if not filtered:
            queryset = queryset.filter(Q(post_contents__publication_date__isnull=True) |
                                       Q(post_contents__isnull=True)).distinct()
        return queryset

    def get_context_data(self, **kwargs):
        context = super(PostListView, self).get_context_data(**kwargs)
        context['post_created_by'] = self.request.GET.get('post_created_by')
        context['post_creators'] = \
            get_user_model().objects.filter(created_social_media_planner_post_set__isnull=False).\
                order_by('last_name', 'first_name').distinct()
        return context


class PostDetailView(LoginRequiredMixin, PostMixin, DetailView):
    template_name = 'social_media_planner%s/post/post_detail.html' % TS

    def get_context_data(self, **kwargs):
        context = super(PostDetailView, self).get_context_data(**kwargs)
        context['post_content_types'] = models.PostContentType.objects.all()
        return context


class PostCreateView(LoginRequiredMixin, PostMixin, CreateView):
    template_name = 'social_media_planner%s/post/post_form.html' % TS

    def get_form_class(self):
        if self.request.GET.get('add_idea') == '1':
            return forms.PostIdeaForm
        return self.form_class

    def get_template_names(self):
        templates = super(PostCreateView, self).get_template_names()
        if self.request.GET.get('add_idea') == '1':
            return ['social_media_planner%s/post/post_idea_form.html' % TS]
        return templates

    def get_initial(self):
        initial = super(PostCreateView, self).get_initial()
        publish_date = self.request.GET.get('publish_date', None)
        if publish_date:
            initial['publish_date'] = datetime.datetime.strptime(publish_date, '%Y-%m-%d').date()
        if self.request.GET.get('add_idea') == '1':
            status = models.ObjectStatus.objects.get(codename='idea',
                model_class_name_tag='social_media_planner.Post.status')
        else:
            status = models.ObjectStatus.objects.get(codename='draft',
                model_class_name_tag='social_media_planner.Post.status')
        initial['status'] = status
        return initial

    def form_valid(self, form):
        post = form.save(commit=False)
        post.audit_user = self.request.user
        post.label = post.label[:150]
        post.save()
        form.save_m2m()
        notifications.notification_action('new_post', self.request.user, post)
        notifications.notification_action('need_my_help', self.request.user, post)
        notifications.notification_action('publish_post', self.request.user, post)
        return HttpResponseRedirect(post.get_absolute_url())

    def get_context_data(self, **kwargs):
        context = super(PostCreateView, self).get_context_data(**kwargs)
        context['key_contents'] = models.KeyContent.objects.all()
        return context


class PostEditView(LoginRequiredMixin, PostMixin, UpdateView):
    template_name = 'social_media_planner%s/post/post_form.html' % TS

    def form_valid(self, form):
        # Get previous helpers and publishers for diff on notification
        old_post = self.get_object()
        previous_helpers = list(old_post.help_needed_from.all())
        previous_publishers = list(old_post.publishers.all())
        # Save edited post
        post = form.save(commit=False)
        post.audit_user = self.request.user
        post.save()
        form.save_m2m()
        notifications.notification_action('edit_my_post', self.request.user, post)
        notifications.notification_action('need_my_help', self.request.user, post,
                                          previous_helpers=previous_helpers)
        notifications.notification_action('publish_post', self.request.user, post,
                                          previous_publishers=previous_publishers)
        return HttpResponseRedirect(post.get_absolute_url())

    def get_context_data(self, **kwargs):
        context = super(PostEditView, self).get_context_data(**kwargs)
        context['key_contents'] = models.KeyContent.objects.all()
        return context


class PostDeleteView(LoginRequiredMixin, PostMixin, DeleteView):
    template_name = 'social_media_planner%s/post/post_confirm_delete.html' % TS


class PostAttachFilesView(LoginRequiredMixin, AttachmentFileUploadReceiveMixin,
                          PostMixin, FormView):
    template_name = 'social_media_planner%s/post/post_attach_files.html' % TS
    form_class = forms.AttachmentsForm

    def get_context_data(self, **kwargs):
        context = super(PostAttachFilesView, self).get_context_data(**kwargs)
        context['post'] = models.Post.objects.get(id=self.kwargs.get('pk'))
        return context

    def form_valid(self, form):
        category = models.ObjectStatus.objects.get(
            model_class_name_tag='social_media_planner.Attachment.category',
            codename='post'
        )
        post = models.Post.objects.get(id=self.kwargs.get('pk'))
        files_dict = []
        for uploaded_file in form.cleaned_data['attachments']:
            file_dict = self.receive(uploaded_file,
                                     overwrite_file=self.request.POST.get('overwrite_file', False))
            files_dict.append(file_dict)
        self.create_attached_files(post, models.Attachment, files_dict, 'attachments',
                                   extra_attributes={'category': category})
        return self.return_response(files_dict)

    def return_response(self, files_dict):
        post = models.Post.objects.get(id=self.kwargs['pk'])
        return HttpResponseRedirect(post.get_absolute_url())


class PostEditAttachmentsView(LoginRequiredMixin, PostMixin, DetailView):
    template_name = 'social_media_planner%s/post/post_edit_attachments.html' % TS

    @staticmethod
    def get_formset():
        return modelformset_factory(models.Attachment, form=forms.AttachmentForm)

    def get_context_data(self, **kwargs):
        context = super(PostEditAttachmentsView, self).get_context_data(**kwargs)
        post = context['post']
        AttachmentsFormSet = self.get_formset()
        formset = AttachmentsFormSet(queryset=post.attachments.all())
        context['attachments_formset'] = formset
        return context

    def post(self, request, *args, **kwargs):
        post = self.get_object()
        AttachmentsFormSet = self.get_formset()
        formset = AttachmentsFormSet(request.POST)
        if formset.is_valid():
            for attachment_form in formset:
                if attachment_form.cleaned_data.get('delete', False):
                    if attachment_form.cleaned_data['id']:
                        attachment = attachment_form.instance
                        attachment.delete()
                else:
                    add_to_post = False
                    attachment = attachment_form.save(commit=False)
                    if not attachment.id:
                        try:
                            mimetype = mimetypes.guess_type(attachment.path)[0]
                            if mimetype is None:
                                mimetype = ''
                        except:
                            mimetype = ''
                        if attachment_form.cleaned_data.get('is_image', True):
                            mimetype = 'image'
                        attachment.file_type = mimetype
                    if not attachment.id:
                        attachment.attachment_type = 'url'
                        add_to_post = True
                    attachment.audit_user = request.user
                    if attachment.name:
                        attachment.save()
                        if add_to_post:
                            post.attachments.add(attachment)
            return HttpResponseRedirect(post.get_absolute_url())

        context = {
            'post': post,
            'attachments_formset': formset,
        }
        return self.render_to_response(context)


"""
class PostAttachedFileDeleteView(LoginRequiredMixin, PostMixin, DetailView):
    template_name = 'social_media_planner%s/post/post_attached_file_confirm_delete.html' % TS

    def get_context_data(self, **kwargs):
        context = super(PostAttachedFileDeleteView, self).get_context_data(**kwargs)
        post = self.get_object()
        context['return_url'] = post.get_absolute_url()
        context['attached_file'] = self.get_attached_file(self.kwargs['attached_file_pk'])
        return context

    def post(self, request, *args, **kwargs):
        post = self.get_object()
        attached_file = self.get_attached_file(kwargs['attached_file_pk'])
        post.attachments.remove(attached_file)
        attached_file.delete()
        return HttpResponseRedirect(post.get_absolute_url())
"""

"""
Post contents
"""


class PostContentMixin(object):
    context_object_name = 'post_content'
    model = models.PostContent
    form_class = forms.PostContentForm

    def get_context_data(self, **kwargs):
        context = super(PostContentMixin, self).get_context_data(**kwargs)
        context['post'] = models.Post.objects.get(id=self.kwargs.get('post_id'))
        return context


class PostContentCreateView(LoginRequiredMixin, PostContentMixin, CreateView):
    template_name = 'social_media_planner%s/post_content/post_content_form.html' % TS

    def get_initial(self):
        initial = super(PostContentCreateView, self).get_initial()
        post_content_type = self.request.GET.get('post-content-type', None)
        if post_content_type:
            try:
                initial['post_content_type'] = \
                    models.PostContentType.objects.get(codename=post_content_type)
            except models.PostContentType.DoesNotExist:
                initial['post_content_type'] = None
        return initial

    def form_valid(self, form):
        post_content = form.save(commit=False)
        post_content.post = models.Post.objects.get(id=self.kwargs.get('post_id'))
        post_content.audit_user = self.request.user
        post_content.save()
        notifications.notification_action('new_post_content', self.request.user, post_content)
        return HttpResponseRedirect(post_content.post.get_absolute_url())


class PostContentEditView(LoginRequiredMixin, PostContentMixin, UpdateView):
    template_name = 'social_media_planner%s/post_content/post_content_form.html' % TS

    def form_valid(self, form):
        post_content = form.save(commit=False)
        post_content.audit_user = self.request.user
        post_content.save()
        notifications.notification_action('edit_my_post_content', self.request.user, post_content)
        return HttpResponseRedirect(post_content.post.get_absolute_url())


class PostContentDeleteView(LoginRequiredMixin, PostContentMixin, DeleteView):
    template_name = 'social_media_planner%s/post_content/post_content_confirm_delete.html' % TS

    def get_success_url(self):
        post_content = self.get_object()
        return post_content.post.get_absolute_url()


class PostContentValidateView(LoginRequiredMixin, PostContentMixin, View):

    def post(self, request, *args, **kwargs):
        post_content = models.PostContent.objects.get(id=self.kwargs.get('pk'))
        type_name = self.kwargs.get('type_name')
        post_content.validate_type(type_name, self.request.user)
        post_content.audit_user = self.request.user
        post_content.save()
        notifications.notification_action('validate_my_post_content', self.request.user,
                                          post_content, type_name=type_name)
        return HttpResponseRedirect(post_content.post.get_absolute_url())


class PostContentInvalidateView(LoginRequiredMixin, PostContentMixin, View):

    def post(self, request, *args, **kwargs):
        post_content = models.PostContent.objects.get(id=self.kwargs.get('pk'))
        type_name = self.kwargs.get('type_name')
        post_content.invalidate_type(type_name)
        post_content.audit_user = self.request.user
        post_content.save()
        return HttpResponseRedirect(post_content.post.get_absolute_url())


class PostContentPublishView(LoginRequiredMixin, PostContentMixin, View):

    def post(self, request, *args, **kwargs):
        post_content = models.PostContent.objects.get(id=self.kwargs.get('pk'))
        post_content.publish(self.request.user)
        post_content.audit_user = self.request.user
        post_content.save()

        post = post_content.post
        all_published = True
        for pc in post.post_contents.all():
            if not pc.is_published:
                all_published = False
                break
        if all_published:
            published_status = models.ObjectStatus.objects.get(
                model_class_name_tag='social_media_planner.Post.status',
                codename='published'
            )
            post.status = published_status
            post.save()
        notifications.notification_action('publish_my_post_content',
                                          self.request.user, post_content)
        return HttpResponseRedirect(post_content.post.get_absolute_url())


"""
Post content reviews
"""


class PostContentReviewMixin(object):
    context_object_name = 'post_content_review'
    model = models.PostContentReview
    form_class = forms.PostContentReviewForm

    def get_context_data(self, **kwargs):
        context = super(PostContentReviewMixin, self).get_context_data(**kwargs)
        context['post'] = models.Post.objects.get(id=self.kwargs.get('post_id'))
        context['post_content'] = \
            models.PostContent.objects.get(id=self.kwargs.get('post_content_id'))
        return context


class PostContentReviewCreateView(LoginRequiredMixin, PostContentReviewMixin, CreateView):
    template_name = 'social_media_planner%s/post_content_review/post_content_review_form.html' % TS

    def get_initial(self):
        initial = super(PostContentReviewCreateView, self).get_initial()
        post_content = models.PostContent.objects.get(id=self.kwargs.get('post_content_id'))
        initial['title'] = post_content.title
        initial['content'] = post_content.content
        return initial

    def form_valid(self, form):
        post_content_review = form.save(commit=False)
        post_content_review.post_content = \
            models.PostContent.objects.get(id=self.kwargs.get('post_content_id'))
        post_content_review.audit_user = self.request.user
        post_content_review.save()
        notifications.notification_action('review_my_post_content',
                                          self.request.user, post_content_review)
        return HttpResponseRedirect(post_content_review.post_content.post.get_absolute_url())


class PostContentReviewEditView(LoginRequiredMixin, PostContentReviewMixin, UpdateView):
    template_name = 'social_media_planner%s/post_content_review/post_content_review_form.html' % TS

    def form_valid(self, form):
        post_content_review = form.save(commit=False)
        post_content_review.audit_user = self.request.user
        post_content_review.save()
        return HttpResponseRedirect(post_content_review.post_content.post.get_absolute_url())


class PostContentReviewDeleteView(LoginRequiredMixin, PostContentReviewMixin, DeleteView):
    template_name = 'social_media_planner%s/post_content_review/' % TS + \
                    'post_content_review_confirm_delete.html'

    def get_success_url(self):
        post_content_review = self.get_object()
        return post_content_review.post_content.post.get_absolute_url()


class PostContentReviewValidateView(LoginRequiredMixin, PostContentReviewMixin, View):

    def post(self, request, *args, **kwargs):
        post_content_review = models.PostContentReview.objects.get(id=self.kwargs.get('pk'))
        type_name = self.kwargs.get('type_name')
        post_content_review.validate_type(type_name, self.request.user)
        post_content_review.audit_user = self.request.user
        post_content_review.save()
        return HttpResponseRedirect(post_content_review.post_content.post.get_absolute_url())


class PostContentReviewInvalidateView(LoginRequiredMixin, PostContentReviewMixin, View):

    def post(self, request, *args, **kwargs):
        post_content_review = models.PostContentReview.objects.get(id=self.kwargs.get('pk'))
        type_name = self.kwargs.get('type_name')
        post_content_review.invalidate_type(type_name)
        post_content_review.audit_user = self.request.user
        post_content_review.save()
        return HttpResponseRedirect(post_content_review.post_content.post.get_absolute_url())


class PostContentReviewSetAsContentView(LoginRequiredMixin, PostContentReviewMixin, View):

    def post(self, request, *args, **kwargs):
        post_content_review = models.PostContentReview.objects.get(id=self.kwargs.get('pk'))
        post_content_review.post_content.content = post_content_review.content
        post_content_review.post_content.title = post_content_review.title
        post_content_review.post_content.audit_user = self.request.user
        post_content_review.post_content.save()
        post_content_review.validate_type('review', self.request.user)
        post_content_review.audit_user = self.request.user
        post_content_review.save()
        return HttpResponseRedirect(post_content_review.post_content.post.get_absolute_url())


"""
User Profile
"""


class UserProfileMixin(object):
    context_object_name = 'user_profile'
    model = models.UserProfile


class UserProfileNotificationsEditView(LoginRequiredMixin, UserProfileMixin, UpdateView):
    template_name = 'social_media_planner%s/user_profile/user_profile_notifications_form.html' % TS
    form_class = forms.UserProfileNotificationsEditForm

    def get_object(self, *args):
        return self.request.user.social_media_planner_profile

    def form_valid(self, form):
        user_profile = form.save(commit=False)
        user_profile.save()
        return HttpResponseRedirect(
            reverse(settings.SOCIAL_MEDIA_PLANNER['GET_ACCOUNT_SETTINGS_URL'](user_profile.user)))
