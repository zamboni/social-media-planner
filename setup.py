from distutils.core import setup

from social_media_planner import VERSION

setup(
    name='social-media-planner',
    version=VERSION,
    description="Django application for social media editorial content",
    author='Vittorio Zamboni',
    author_email='vittorio.zamboni@gmail.com',
    license='MIT',
    url='http://bitbucket.org/zamboni/social-media-planner',
    packages=[
        'social_media_planner',
        'social_media_planner.fixtures_module',
        'social_media_planner.migrations',
    ],
    dependency_links=[
        'https://bitbucket.org/zamboni/django-helper-forms/get/tip.zip#egg=django_helper_forms',
        'https://bitbucket.org/zamboni/django-utils/get/tip.zip#egg=django_utils',
        'https://github.com/vittoriozamboni/django-model-attachments/archive/master.zip#egg=django_model_attachments',
        'https://github.com/yigor/django-jsonfield/archive/master.zip#egg=jsonfield',
    ],
    install_requires=[
        'bleach>=1.4.2',
        'django-mailer>=1.1',
        'django>=1.9',
        'django-braces>=1.8',
        'python-dateutil>=2.4.2.',
        'django-multiupload>=0.5',
        'django-mptt>=0.8.0',
        'django-taggit>=0.17.6',
        'django-widget-tweaks>=1.4',
    ],
)
