from __future__ import unicode_literals

from django.apps import AppConfig


class SocialMediaPlannerConfig(AppConfig):
    name = 'social_media_planner'
