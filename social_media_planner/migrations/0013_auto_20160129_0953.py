# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-29 09:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('social_media_planner', '0012_auto_20160124_1831'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='notification_delete_my_post_content',
            field=models.BooleanField(default=True, help_text='Send me a notification when someone deletes one of my post contents'),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='notification_meeting_day',
            field=models.BooleanField(default=True, help_text='Send me a notification the day of the meeting as a reminder'),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='notification_new_meeting',
            field=models.BooleanField(default=True, help_text='Send me a notification when someone creates a new meeting'),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='notification_weekly_reminder',
            field=models.BooleanField(default=True, help_text='Send me a notification every week with a reminder of posts'),
        ),
    ]
