from __future__ import unicode_literals

import datetime

from django.conf import settings as dj_settings
from django.contrib.auth.signals import user_logged_in
from django.core.urlresolvers import reverse
from django.db import models

from mptt.models import MPTTModel, TreeForeignKey
from slugify import slugify
from taggit.managers import TaggableManager

from django_utils.models import AuditLogMixin, ObjectStatus
from model_attachments.models import AttachmentMixin

import settings


POST_CONTENT_REVIEW_TYPES = (
    ('note', 'Note'),
    ('important', 'Important'),
    ('warning', 'Warning'),
    ('rewrite', 'Rewrite'),
)


class Attachment(AttachmentMixin):
    category = models.ForeignKey(ObjectStatus, null=True, blank=True,
        limit_choices_to={'model_class_name_tag': 'social_media_planner.Attachment.category'},
        related_name='social_media_planner_attachments')

    class Meta:
        ordering = ('file_type', 'name')

    def get_absolute_url(self):
        return reverse('social_media_planner:meeting_detail', kwargs={'pk': self.pk})


class MeetingManager(models.Manager):

    def get_next_meeting(self):
        try:
            return super(MeetingManager, self).filter(meeting_date__gte=datetime.datetime.now())[0]
        except IndexError:
            return None


class Meeting(AuditLogMixin):
    title = models.CharField(max_length=200)
    meeting_date = models.DateTimeField()
    participants = models.ManyToManyField(getattr(dj_settings, 'AUTH_USER_MODEL', 'auth.User'),
                                          related_name='social_media_planner_meetings', blank=True)
    topics = models.TextField(default='', blank=True)
    short_report = models.TextField(default='', blank=True)

    objects = MeetingManager()

    def __str__(self):
        return '{title} ({meeting_date})'.\
            format(title=self.title, meeting_date=str(self.meeting_date))

    def __unicode__(self):
        return u'{title} ({meeting_date})'.\
            format(title=self.title, meeting_date=str(self.meeting_date))

    def get_absolute_url(self):
        return reverse('social_media_planner:meeting_detail', kwargs={'pk': self.pk})


class KeyContent(MPTTModel):
    label = models.CharField(max_length=100)
    codename = models.CharField(max_length=50, default='', blank=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='subkeys', db_index=True)
    key_content_color = models.CharField(max_length=7, default='', blank=True)
    key_content_text_color = models.CharField(max_length=7, default='#FFFFFF', blank=True,
        help_text='Reverse color (if color is used as background), default is white')

    def __str__(self):
        label = '{label}'.format(label=self.label)
        if self.parent:
            label += ' ({label_path})'.format(label_path=self.parents_label_path)
        return label

    def __unicode__(self):
        label = '{label}'.format(label=self.label)
        if self.parent:
            label += ' ({label_path})'.format(label_path=self.parents_label_path)
        return unicode(label)

    def save(self, *args, **kwargs):
        if not self.codename:
            self.codename = slugify(self.label, to_lower=True)
        super(KeyContent, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('social_media_planner:key_content_detail', kwargs={'pk': self.pk})

    @property
    def parents_label_path(self):
        if not self.parent:
            return ''
        labels = []
        parent = self.parent
        while parent:
            labels.append(parent.label)
            parent = parent.parent
        return ' > '.join(sorted(labels, reverse=True))

    @property
    def color(self):
        if self.key_content_color:
            return self.key_content_color
        key_content_color = ''
        parent = self.parent
        while key_content_color == '' and parent:
            key_content_color = parent.key_content_color
            parent = parent.parent
        return key_content_color


class Category(models.Model):
    """ i.e. Events, Product launches, Promotions, Monthly marketing headlines
    """
    label = models.CharField(max_length=100)
    codename = models.CharField(max_length=50, default='', blank=True)
    icon = models.CharField(max_length=50, default='', blank=True)

    class Meta:
        verbose_name_plural = 'categories'

    def __str__(self):
        return '{label}'.format(label=self.label)

    def __unicode__(self):
        return u'{label}'.format(label=self.label)

    def save(self, *args, **kwargs):
        if not self.codename:
            self.codename = slugify(self.label, to_lower=True)
        super(Category, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('social_media_planner:category_detail', kwargs={'pk': self.pk})


class PostContentType(models.Model):
    """ i.e. Twitter, Facebook, Linkedin
    - `icon`: icon name (i.e. 'fa-twitter')
    """
    label = models.CharField(max_length=100)
    codename = models.CharField(max_length=50, default='', blank=True)
    icon = models.CharField(max_length=50, default='', blank=True)

    def __str__(self):
        return '{label}'.format(label=self.label)

    def __unicode__(self):
        return u'{label}'.format(label=self.label)

    def save(self, *args, **kwargs):
        if not self.codename:
            self.codename = slugify(self.label, to_lower=True)
        super(PostContentType, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('social_media_planner:post_content_type_detail', kwargs={'pk': self.pk})


class Post(AuditLogMixin):
    label = models.CharField(max_length=200)
    post_reason = models.TextField(default='', blank=True,
        help_text='What\'s going on / content themes')
    publish_date = models.DateField(null=True, blank=True)
    publish_date_from = models.DateField(null=True, blank=True)
    publish_date_to = models.DateField(null=True, blank=True)
    status = models.ForeignKey(ObjectStatus, null=True, blank=True,
        limit_choices_to={'model_class_name_tag': 'social_media_planner.Post.status'},
        related_name='post_statuses')
    expected_impact = models.PositiveSmallIntegerField(
        default=1, help_text='1 (low impact) to 10 (high impact)')
    feedback = models.TextField(default='', blank=True, help_text='Insights, results')
    help_needed = models.BooleanField(default=False)
    help_needed_from = models.ManyToManyField(
        getattr(dj_settings, 'AUTH_USER_MODEL', 'auth.User'),
        related_name='social_media_planner_help_with_posts', blank=True)
    publishers = models.ManyToManyField(
        getattr(dj_settings, 'AUTH_USER_MODEL', 'auth.User'),
        related_name='social_media_planner_publisher_posts', blank=True)

    tags = TaggableManager(blank=True)
    key_content = models.ForeignKey(KeyContent)
    category = models.ForeignKey(Category)
    attachments = models.ManyToManyField(Attachment, blank=True)

    class Meta:
        permissions = (
            ("can_validate_post_grammar", "Can validate post grammar"),
            ("can_validate_post_content", "Can validate post content"),
            ("can_validate_post_post_content_type", "Can validate post content type"),
            ("can_manage_post_content_reviews", "Can manage post content reviews"),
            ("can_publish_post", "Can publish post"),
            ("can_manage_post_attachments", "Can manage attachments to post"),
        )

    def __str__(self):
        return '{label}'.format(label=self.label)

    def __unicode__(self):
        return u'{label}'.format(label=self.label)

    def save(self, *args, **kwargs):
        self.expected_impact = min(10, max(0, self.expected_impact))
        super(Post, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('social_media_planner:post_detail', kwargs={'pk': self.pk})

    @property
    def attached_files_path(self):
        return settings.SOCIAL_MEDIA_PLANNER['PATHS']['post'].\
            format(id=self.id, media=dj_settings.MEDIA_ROOT)

    def get_attachment_delete_url(self, attached_file_pk):
        return reverse('social_media_planner:post_attached_file_delete',
                       kwargs={'pk': self.pk, 'attached_file_pk': attached_file_pk})


class PostContent(AuditLogMixin):
    """
    - `content_checked`: the post content has been approved
    - `grammar_checked`: the grammar is ok
    - `shares`: approx. number of shares on social
    - `favourites`: approx. number of favourites or likes on social
    """
    post = models.ForeignKey(Post, related_name='post_contents')
    title = models.CharField(
        max_length=200, default='', blank=True, help_text='Title to be used in final post')
    content = models.TextField(default='', blank=True)
    content_notes = models.TextField(default='', blank=True, help_text="Revision notes on content")
    post_content_type = models.ForeignKey(PostContentType)
    language = models.CharField(max_length=7, choices=dj_settings.LANGUAGES, default='en')
    final_link = models.TextField(default='', blank=True)
    content_checked = models.BooleanField(default=False)
    content_checked_by = models.ForeignKey(
        getattr(dj_settings, 'AUTH_USER_MODEL', 'auth.User'), null=True, blank=True,
        related_name='posts_content_checked')
    content_checked_date = models.DateField(null=True, blank=True)
    grammar_checked = models.BooleanField(default=False)
    grammar_checked_by = models.ForeignKey(
        getattr(dj_settings, 'AUTH_USER_MODEL', 'auth.User'), null=True, blank=True,
        related_name='posts_grammar_checked')
    grammar_checked_date = models.DateField(null=True, blank=True)
    post_content_type_checked = models.BooleanField(default=False)
    post_content_type_checked_by = models.ForeignKey(
        getattr(dj_settings, 'AUTH_USER_MODEL', 'auth.User'), null=True, blank=True,
        related_name='post_content_types_checked')
    post_content_type_checked_date = models.DateField(null=True, blank=True)
    shares = models.IntegerField(default=0, blank=True)
    favourites = models.IntegerField(default=0, blank=True)

    publication_date = models.DateTimeField(null=True, blank=True)
    publicated_by = models.ForeignKey(
        getattr(dj_settings, 'AUTH_USER_MODEL', 'auth.User'), null=True, blank=True,
        related_name='publicated_posts')

    class Meta:
        ordering = ['creation_date']

    def __str__(self):
        return '{title}'.format(title=self.title)

    def __unicode__(self):
        return u'{title}'.format(title=self.title)

    @property
    def is_valid(self):
        return self.content_checked and self.grammar_checked and self.post_content_type_checked

    def validate_type(self, type_name, user):
        setattr(self, type_name + '_checked', True)
        setattr(self, type_name + '_checked_by', user)
        setattr(self, type_name + '_checked_date', datetime.date.today())

    def invalidate_type(self, type_name):
        setattr(self, type_name + '_checked', False)
        setattr(self, type_name + '_checked_by', None)
        setattr(self, type_name + '_checked_date', None)

    def validate_grammar(self, user):
        self.validate_type('grammar', user)

    def invalidate_grammar(self):
        self.invalidate_type('grammar')

    def validate_content(self, user):
        self.validate_type('content', user)

    def invalidate_content(self):
        self.invalidate_type('content')

    def validate_post_content_type(self, user):
        self.validate_type('post_content_type', user)

    def invalidate_post_content_type(self):
        self.invalidate_type('post_content_type')
    
    @property
    def is_published(self):
        return self.publication_date is not None

    def publish(self, user):
        self.publicated_by = user
        self.publication_date = datetime.datetime.now()


class PostContentReview(AuditLogMixin):
    post_content = models.ForeignKey(PostContent, related_name='post_content_reviews')
    title = models.CharField(max_length=200, default='', blank=True, help_text='Suggest new title')
    content = models.TextField(default='', blank=True, help_text='')
    review_type = models.CharField(max_length=30, default='note',
        choices=POST_CONTENT_REVIEW_TYPES, help_text='')
    review_checked = models.BooleanField(default=False)
    review_checked_by = models.ForeignKey(
        getattr(dj_settings, 'AUTH_USER_MODEL', 'auth.User'), null=True, blank=True,
        related_name='post_content_reviews_checked')
    review_checked_date = models.DateField(null=True, blank=True)

    class Meta:
        ordering = ('post_content', 'creation_date')

    def __str__(self):
        if not self.pk:
            return 'unsaved review for post content {post_content}'.\
                format(post_content=self.post_content)
        return '{post_content} - {created_by} - {creation_date}'.\
            format(post_content=self.post_content,
            created_by=self.created_by.get_full_name(), creation_date=self.creation_date)

    def __unicode__(self):
        if not self.pk:
            return u'unsaved review for post content {post_content}'.\
                format(post_content=self.post_content)
        return u'{post_content} - {created_by} - {creation_date}'.\
            format(post_content=self.post_content,
                   created_by=self.created_by.get_full_name(), creation_date=self.creation_date)

    def save(self, *args, **kwargs):
        if not self.created_by and not self.audit_user:
            raise AttributeError('created_by or audit_user attributes must be set before save.')
        super(PostContentReview, self).save(*args, **kwargs)

    def validate_type(self, type_name, user):
        setattr(self, type_name + '_checked', True)
        setattr(self, type_name + '_checked_by', user)
        setattr(self, type_name + '_checked_date', datetime.date.today())

    def invalidate_type(self, type_name):
        setattr(self, type_name + '_checked', False)
        setattr(self, type_name + '_checked_by', None)
        setattr(self, type_name + '_checked_date', None)

    def validate_review(self, user):
        self.validate_type('review', user)

    def invalidate_review(self):
        self.invalidate_type('review')


class UserProfile(models.Model):
    user = models.OneToOneField(getattr(dj_settings, 'AUTH_USER_MODEL', 'auth.User'),
        related_name='social_media_planner_profile')
    notification_new_meeting = models.BooleanField(default=True,
        help_text='Send me a notification when someone creates a new meeting')
    notification_meeting_day = models.BooleanField(default=True,
        help_text='Send me a notification the day of the meeting as a reminder')
    notification_new_post = models.BooleanField(default=True,
        help_text='Send me a notification when someone adds a post')
    notification_edit_my_post = models.BooleanField(default=True,
        help_text='Send me a notification when someone edits one of my posts')
    notification_delete_my_post = models.BooleanField(default=True,
        help_text='Send me a notification when someone deletes one of my posts')
    notification_new_post_content = models.BooleanField(default=True,
        help_text='Send me a notification when someone adds a post content to one of my posts')
    notification_edit_my_post_content = models.BooleanField(default=True,
        help_text='Send me a notification when someone edits one of my post contents')
    notification_delete_my_post_content = models.BooleanField(default=True,
        help_text='Send me a notification when someone deletes one of my post contents')
    notification_review_my_post_content = models.BooleanField(default=True,
        help_text='Send me a notification when someone adds review in one of my post contents')
    notification_validate_my_post_content = models.BooleanField(default=True,
        help_text='Send me a notification when someone validates one of my post contents')
    notification_publish_my_post_content = models.BooleanField(default=True,
        help_text='Send me a notification when someone publish one of my post contents')
    notification_read_my_review = models.BooleanField(default=True,
        help_text='Send me a notification when someone reads one of my reviews')
    notification_need_my_help = models.BooleanField(default=True,
        help_text='Send me a notification when someone needs my help')
    notification_publish_post = models.BooleanField(default=True,
        help_text='Send me a notification when I have to publish a post')
    notification_publish_post_reminder = models.BooleanField(default=True,
        help_text='Send me a notification the day when I have to publish a post')
    notification_weekly_reminder = models.BooleanField(default=True,
        help_text='Send me a notification every week with a reminder of posts')

    def __str__(self):
        return '{username} profile'.format(username=self.user.username)

    def __unicode__(self):
        return u'{username} profile'.format(username=self.user.username)


def create_user_profile(sender, request, user, **kwargs):
    UserProfile.objects.get_or_create(user=user)

user_logged_in.connect(create_user_profile)
