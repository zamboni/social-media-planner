import datetime

from django.contrib.auth import get_user_model
from django.test import TestCase
from social_media_planner import models


class UserTestMixin(TestCase):
    def setUp(self):
        super(UserTestMixin, self).setUp()
        user_model = get_user_model()
        self.test_user = user_model(username='test_user', first_name='Test', last_name='User')
        self.test_user.save()


class MeetingTestCase(TestCase):
    def setUp(self):
        self.today_date = datetime.datetime.now()
        models.Meeting.objects.create(title='First meeting', meeting_date=self.today_date)
        models.Meeting.objects.create(title='Future meeting',
                                      meeting_date=self.today_date + datetime.timedelta(days=1))

    def test_string_and_unicode(self):
        first_meeting = models.Meeting.objects.get(title='First meeting')
        self.assertEqual(str(first_meeting), 'First meeting ({meeting_date})'.format(meeting_date=self.today_date))
        self.assertEqual(unicode(first_meeting), u'First meeting ({meeting_date})'.format(meeting_date=self.today_date))

    def test_next_meeting(self):
        next_meeting = models.Meeting.objects.get_next_meeting()
        self.assertEqual(next_meeting.title, 'Future meeting')
        next_meeting.delete()
        next_meeting = models.Meeting.objects.get_next_meeting()
        self.assertIsNone(next_meeting)


class KeyContentTestMixin(TestCase):
    def setUp(self):
        super(KeyContentTestMixin, self).setUp()
        self.human = models.KeyContent(label='Human NGS')
        self.human.save()
        self.human_education = models.KeyContent(label='Education', parent=self.human, key_content_color='#00FF00')
        self.human_education.save()
        self.human_education_origin = models.KeyContent(label='Origin', parent=self.human_education)
        self.human_education_origin.save()


class KeyContentTestCase(KeyContentTestMixin):

    def test_string_and_unicode(self):
        self.assertEqual(str(self.human_education_origin), 'Origin (Human NGS > Education)')
        self.assertEqual(unicode(self.human_education_origin), u'Origin (Human NGS > Education)')

    def test_color(self):
        self.assertEqual(self.human_education_origin.color, '#00FF00')


class CategoryTestMixin(TestCase):
    def setUp(self):
        super(CategoryTestMixin, self).setUp()
        self.events = models.Category(label='Events')
        self.events.save()


class CategoryTestCase(CategoryTestMixin):

    def test_string_and_unicode(self):
        self.assertEqual(str(self.events), 'Events')
        self.assertEqual(unicode(self.events), u'Events')


class PostContentTypeTestMixin(TestCase):
    def setUp(self):
        self.twitter = models.PostContentType(label='Twitter', icon='fa-twitter')
        self.twitter.save()


class PostContentTypeTestCase(PostContentTypeTestMixin):

    def test_string_and_unicode(self):
        self.assertEqual(str(self.twitter), 'Twitter')
        self.assertEqual(unicode(self.twitter), u'Twitter')


class PostTestMixin(KeyContentTestMixin, CategoryTestMixin, UserTestMixin):

    def setUp(self):
        super(PostTestMixin, self).setUp()
        self.twitter_post = models.Post(
            label='Example',
            category=self.events,
            key_content=self.human_education_origin,
            publish_date=datetime.date.today()
        )
        self.twitter_post.save()


class PostTestCase(PostTestMixin):

    def test_string_and_unicode(self):
        self.assertEqual(str(self.twitter_post), 'Example')
        self.assertEqual(unicode(self.twitter_post), u'Example')

class PostContentTestMixin(PostTestMixin, PostContentTypeTestMixin):

    def setUp(self):
        super(PostContentTestMixin, self).setUp()
        self.twitter_content = models.PostContent(
            post=self.twitter_post,
            title='Twitter content',
            content='This is a twitter content',
            post_content_type=self.twitter
        )
        self.twitter_content.save()

class PostContentTestCase(PostContentTestMixin):

    def test_string_and_unicode(self):
        self.assertEqual(str(self.twitter_content), 'Twitter content')
        self.assertEqual(unicode(self.twitter_content), u'Twitter content')

    def test_validation_and_invalidation(self):

        self.twitter_content.validate_content(self.test_user)
        self.assertEqual(self.twitter_content.content_checked, True)
        self.assertEqual(self.twitter_content.content_checked_by, self.test_user)
        self.assertEqual(self.twitter_content.content_checked_date, datetime.date.today())
    
        self.twitter_content.validate_grammar(self.test_user)
        self.assertEqual(self.twitter_content.grammar_checked, True)
        self.assertEqual(self.twitter_content.grammar_checked_by, self.test_user)
        self.assertEqual(self.twitter_content.grammar_checked_date, datetime.date.today())

        self.twitter_content.validate_post_content_type(self.test_user)
        self.assertEqual(self.twitter_content.post_content_type_checked, True)
        self.assertEqual(self.twitter_content.post_content_type_checked_by, self.test_user)
        self.assertEqual(self.twitter_content.post_content_type_checked_date, datetime.date.today())

        self.assertTrue(self.twitter_content.is_valid)

        self.twitter_content.invalidate_content()
        self.assertEqual(self.twitter_content.content_checked, False)
        self.assertEqual(self.twitter_content.content_checked_by, None)
        self.assertEqual(self.twitter_content.content_checked_date, None)

        self.twitter_content.invalidate_grammar()
        self.assertEqual(self.twitter_content.grammar_checked, False)
        self.assertEqual(self.twitter_content.grammar_checked_by, None)
        self.assertEqual(self.twitter_content.grammar_checked_date, None)

        self.twitter_content.invalidate_post_content_type()
        self.assertEqual(self.twitter_content.post_content_type_checked, False)
        self.assertEqual(self.twitter_content.post_content_type_checked_by, None)
        self.assertEqual(self.twitter_content.post_content_type_checked_date, None)

        self.assertFalse(self.twitter_content.is_valid)

    def test_publish(self):
        self.twitter_content.publish(self.test_user)
        self.assertIsNotNone(self.twitter_content.publication_date)
        self.assertEqual(self.twitter_content.publicated_by, self.test_user)

class PostContentReviewTestMixin(PostContentTestMixin):

    def setUp(self):
        super(PostContentReviewTestMixin, self).setUp()
        self.twitter_content_review = models.PostContentReview(
            post_content=self.twitter_content,
            title='Twitter content',
            content='This is a twitter content'
        )
        self.twitter_content_review.audit_user = self.test_user
        self.twitter_content_review.save()

    def test_string_and_unicode(self):
        unsaved_review = models.PostContentReview(post_content=self.twitter_content, content='FB content')
        self.assertEqual(str(unsaved_review), 'unsaved review for post content Twitter content')
        self.assertEqual(unicode(unsaved_review), u'unsaved review for post content Twitter content')

        self.assertEqual(str(self.twitter_content_review),
                         'Twitter content - {created_by} - {creation_date}'.format(
                             created_by=self.twitter_content_review.created_by.get_full_name(),
                             creation_date=self.twitter_content_review.creation_date
        ))
        self.assertEqual(unicode(self.twitter_content_review),
                         u'Twitter content - {created_by} - {creation_date}'.format(
                             created_by=self.twitter_content_review.created_by.get_full_name(),
                             creation_date=self.twitter_content_review.creation_date))

    def test_validation_and_invalidation(self):
        self.twitter_content_review.validate_review(self.test_user)
        self.assertEqual(self.twitter_content_review.review_checked, True)
        self.assertEqual(self.twitter_content_review.review_checked_by, self.test_user)
        self.assertEqual(self.twitter_content_review.review_checked_date, datetime.date.today())

        self.twitter_content_review.invalidate_review()
        self.assertEqual(self.twitter_content_review.review_checked, False)
        self.assertEqual(self.twitter_content_review.review_checked_by, None)
        self.assertEqual(self.twitter_content_review.review_checked_date, None)
