# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-01-24 18:06
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('social_media_planner', '0010_auto_20160124_1315'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('notification_new_post', models.BooleanField(default=True, help_text='Receive a notification when someone adds a post')),
                ('notification_edit_my_post', models.BooleanField(default=True, help_text='Receive a notification when someone edits one of my posts')),
                ('notification_delete_my_post', models.BooleanField(default=True, help_text='Receive a notification when someone deletes one of my posts')),
                ('notification_new_post_content', models.BooleanField(default=False, help_text='Receive a notification when someone adds a post content to one of my posts')),
                ('notification_edit_my_post_content', models.BooleanField(default=True, help_text='Receive a notification when someone edits one of my post contents')),
                ('notification_review_my_post_content', models.BooleanField(default=True, help_text='Receive a notification when someone adds review in one of my post contents')),
                ('notification_validate_my_post_content', models.BooleanField(default=True, help_text='Receive a notification when someone validates one of my post contents')),
                ('notification_publish_my_post_content', models.BooleanField(default=True, help_text='Receive a notification when someone publish one of my post contents')),
                ('notification_read_my_review', models.BooleanField(default=True, help_text='Receive a notification when someone reads one of my reviews')),
                ('notification_need_my_help', models.BooleanField(default=True, help_text='Receive a notification when someone needs my help')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='social_media_planner_profile', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterModelOptions(
            name='post',
            options={'permissions': (('can_validate_post_grammar', 'Can validate post grammar'), ('can_validate_post_content', 'Can validate post content'), ('can_validate_post_post_content_type', 'Can validate post content type'), ('can_manage_post_content_reviews', 'Can manage post content reviews'), ('can_publish_post', 'Can publish post'))},
        ),
        migrations.AlterModelOptions(
            name='postcontentreview',
            options={'ordering': ('post_content', 'creation_date')},
        ),
        migrations.AlterField(
            model_name='postcontentreview',
            name='review_type',
            field=models.CharField(choices=[('note', 'Note'), ('important', 'Important'), ('warning', 'Warning')], default='note', max_length=30),
        ),
    ]
